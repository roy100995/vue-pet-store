import Vue from 'vue'
import Vuex from 'vuex'
import PetService from '@/services/PetService'
import UserService from '@/services/UserService'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    token: null,
    user: null,
    loadingStatus: false,
    links: [
      {
        name: 'Inicio',
        route: '/',
        icon: ''
      },
      {
        name: 'Nosotros',
        route: '/nosotros',
        icon: ''
      },
      {
        name: 'Adopta',
        route: '/pets',
        icon: ''
      },
      {
        name: 'Dona',
        route: '/dona',
        icon: ''
      },
      {
        name: '',
        route: '/login',
        icon: 'mdi-account-circle'
      }
    ],
    pets: [],
    pet: {},
    types: [],
    adoption: {}
  },

  mutations: {
    SET_PETS(state, pets) {
      state.pets = pets
    },
    SET_PET(state, pet) {
      state.pet = pet
    },
    SET_TYPES(state, types) {
      state.types = types
    },
    SET_TOKEN(state, token) {
      state.token = token
    },
    SET_USER(state, user) {
      state.user = user
    },
    SET_ROUTE(state, route) {
      state.links[4].route = route
    },
    SET_ADOPTION(state, adoption) {
      state.adoption = adoption
    }
  },
  actions: {
    fetchPets({ commit }, { type }) {
      PetService.getPets(type)
        .then((response) => {
          commit('SET_PETS', response.data)
        })
        .catch((e) => {
          console.log('There was an error: ', e)
        })
    },
    fetchPet({ commit, getters, state }, id) {
      if (id === state.pet.id) {
        return state.pet
      }
      let pet = getters.getPetById(id)

      if (pet) {
        commit('SET_PET', pet)
        return pet
      } else {
        return PetService.getPet(id).then((res) => {
          commit('SET_PET', res.data)
          return res.data
        })
      }
    },
    postPet({ commit }, pet) {
      PetService.postPet(pet)
        .then((response) => {
          commit('SET_PET', response.data)
        })
        .catch((e) => {
          console.log(`Hubo un error al registrar la mascota: ${e}`)
        })
    },
    fetchTypes({ commit }) {
      PetService.getTypes()
        .then((response) => {
          commit('SET_TYPES', response.data)
        })
        .catch((e) => {
          console.log('There was an error: ', e)
        })
    },
    userLogin({ commit }, { username, password }) {
      UserService.login({
        username,
        password
      })
        .then((response) => {
          const key = response.data.key
          commit('SET_TOKEN', key)
          console.log(key)
          UserService.getUser(response.data.key).then((response) => {
            commit('SET_USER', response.data[0])
            commit('SET_ROUTE', '/account')
          })
        })
        .catch((e) => {
          console.log(`Login error ${e}`)
        })
    },
    userRegister({ commit }, { username, password1, password2 }) {
      UserService.registration({
        username,
        password1,
        password2
      })
        .then((response) => {
          const key = response.data.key
          commit('SET_TOKEN', key)
          console.log(key)
          UserService.getUser(response.data.key).then((response) => {
            const user = response.data[0]
            console.log(user)
            commit('SET_USER', user)
            commit('SET_ROUTE', '/account')
          })
        })
        .catch((e) => {
          console.log(`Signup error: ${e}`)
        })
    },
    adopt({ commit }, { pet, person }) {
      PetService.adopt({
        pet,
        person
      }).then((response) => {
        commit('SET_ADOPTION', response.data)
      })
    }
  },

  modules: {},

  getters: {
    getPetById: (state) => (id) => {
      return state.pets.find((pet) => id === pet.id)
    }
  }
})
