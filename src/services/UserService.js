import axios from 'axios'

const apiClient = axios.create({
  baseURL: `http://127.0.0.1:8000/api`,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  login(user) {
    return apiClient.post('/rest-auth/login/', user)
  },
  registration(user) {
    return apiClient.post('/rest-auth/registration/', user)
  },
  getUser(token) {
    console.log(token)
    return apiClient.get('/users/', {
      headers: { Authorization: `Token ${token}` }
    })
  }
}
