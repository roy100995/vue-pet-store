import axios from 'axios'

const apiClient = axios.create({
  baseURL: `http://127.0.0.1:8000/api`,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
})

export default {
  getPets(type) {
    if (type === 0) {
      return apiClient.get('/pets')
    } else {
      return apiClient.get('/pets/?type=' + type)
    }
  },
  getPet(id) {
    return apiClient.get('/pets/' + id)
  },
  postPet(pet) {
    return apiClient.post('/pets/', pet)
  },
  getTypes() {
    return apiClient.get('/types')
  },
  adopt(adoption) {
    return apiClient.post('/adopts/', adoption)
  }
}
