import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home'
import About from '../views/About'
import Img from '../components/CardComponent'
import Dogs from '../views/Dogs'
import Cats from '../views/Cats'
import Pets from '../views/Pets'
import Adopt from '../views/Adopt'
import Login from '../views/Login'
import Signup from '../views/Signup'
import Account from '../views/Account'
import store from '../store'
import NotFound from '../views/NotFound'
import NetworkIssue from '../views/NetworkIssue'
import PostPet from '../views/PostPet'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/nosotros',
    name: 'nosotros',
    component: About
  },
  {
    path: '/img',
    name: 'img',
    component: Img
  },
  {
    path: '/dogs',
    name: 'dogs',
    component: Dogs
  },
  {
    path: '/cats',
    name: 'cats',
    component: Cats
  },
  {
    path: '/pets',
    name: 'pets',
    component: Pets
  },
  {
    path: '/adopt/:id',
    name: 'adopt',
    component: Adopt,
    props: true,
    beforeEnter(routeTo, routeFrom, next) {
      store
        .dispatch('fetchPet', routeTo.params.id)
        .then((pet) => {
          routeTo.params.pet = pet
          next()
        })
        .catch((e) => {
          if (e.response && e.response.status === 404) {
            next({ name: '404', params: { resource: 'pet' } })
          } else {
            next({ name: 'network-issue' })
          }
        })
    }
  },
  {
    path: '/404',
    name: '404',
    component: NotFound,
    props: true
  },
  {
    path: '/network-issue',
    name: 'network-issue',
    component: NetworkIssue
  },
  {
    path: '/login',
    name: 'login',
    component: Login
  },
  {
    path: '/signup',
    name: 'signup',
    component: Signup
  },
  {
    path: '/account',
    name: 'account',
    component: Account
  },
  {
    path: '/post-pet',
    name: 'post-pet',
    component: PostPet
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
